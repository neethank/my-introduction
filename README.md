![IMG_20201009_211235_323](/uploads/0eee9c9439526a28252d2cda5a6c402e/IMG_20201009_211235_323.jpg)
#MYSELF :stuck_out_tongue:.

Hello World, I'm _**Navaneetha Krishna Chandran**_. I'm an _**Aerospace Engineering**_ student in _**Universiti Putra Malaysia**_. I'm from **Johor Bahru, Johor**- the homeland of _HARIMAU SELATAN_. 

##MY QUALITIES :muscle_tone1:.

Basically I'm a **talkative person** who find easy to talk to people. Well this makes me to do well in teamworks. I find myself as a **person who is ready to bear huge responsibility** because as per now I already handling my studies and my family responsibilities at the same time. Lastly, I am a **dedicated person**, as I am willing to give my full effort into the things that I'm interested. That's all. 

Strength | Weakness
-------- | --------
Hardworking | Careless
Kind | Easy to be fooled

#MY INTEREST :sparkles:. 

* Cars
* Machines
* Travelling
* Aircrafts(_especially drones_)
* AR Rahman musics 
  - My most favourite song [Macho from Mersal](https://youtu.be/MmvpbLdaIRs)

  This is all about me !!!
